import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15


Item {
    property int state : stateEnum.defaultState
    property int bone : bonesEnum.defaultState
	property int expectedWidth: 94
	property int expectedHeight: 94

    implicitWidth: 94
    implicitHeight: 94

    QtObject {
        id: stateEnum

        property int defaultState: 0
        property int hover: 1
    }

    QtObject {
        id: bonesEnum

        property int defaultState: 0
        property int all: 1
        property int leftFemur: 2
        property int rightFemur: 3
        property int leftHemiPelvis: 4
        property int rightHemiPelvis: 5
    }

    // Private

    id: root

    onStateChanged: {
        stackLayout.currentIndex = mapStateToIndex(root.state, root.bone)
    }

    onBoneChanged: {
        stackLayout.currentIndex = mapStateToIndex(root.state, root.bone)
    }

    function mapStateToIndex(selectedState, selectedBone)
    {
        switch (selectedState) {
        case stateEnum.defaultState:
            switch (selectedBone) {
                case bonesEnum.defaultState:
                    return 0
                case bonesEnum.all:
                    return 2
                case bonesEnum.leftFemur:
                    return 3
                case bonesEnum.rightFemur:
                    return 7
                case bonesEnum.leftHemiPelvis:
                    return 5
                case bonesEnum.rightHemiPelvis:
                    return 9
            }
            return 11
        case stateEnum.hover:
            switch (selectedBone) {
                case bonesEnum.defaultState:
                    return 1
                case bonesEnum.all:
                    return -1 // no image for this (yet)
                case bonesEnum.leftFemur:
                    return 4
                case bonesEnum.rightFemur:
                    return 8
                case bonesEnum.leftHemiPelvis:
                    return 6
                case bonesEnum.rightHemiPelvis:
                    return 10
            }
            return 11
        }

        return 11
    }

    StackLayout {
        id: stackLayout

        anchors.fill: parent

        currentIndex: 3

        Image { // 0
            source: "Selected Bone=Default, State=Default hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            // This has to be fixed at the size where the icon is actually shown.
            // Otherwise it either is not smooth (if number is too big) or blury (if number is to low)
            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 1
            source: "Selected Bone=Default, State=Hover hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 2
            source: "Selected Bone=All, State=Default hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 3
            source: "Selected Bone=Left Femur, State=Default hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 4
            source: "Selected Bone=Left Femur, State=Hover hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 5
            source: "Selected Bone=Left Hemipelvis, State=Default hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 6
            source: "Selected Bone=Left Hemipelvis, State=Hover hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 7
            source: "Selected Bone=Right Femur, State=Default hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 8
            source: "Selected Bone=Right Femur, State=Hover hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 9
            source: "Selected Bone=Right Hemipelvis, State=Default hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Image { // 10
            source: "Selected Bone=Right Hemipelvis, State=Hover hip-selector.svg"
            fillMode: Image.PreserveAspectFit

            sourceSize.width: root.expectedWidth
            sourceSize.height: root.expectedHeight
        }

        Item { // 11 - error frame. shown if no image matches
            implicitWidth: root.expectedWidth
            implicitHeight: root.expectedHeight
        }
    }
}