/***********************************************************
 ***   THIS DOCUMENT CONTAINS PROPRIETARY INFORMATION.   ***
 ***    IT IS THE EXCLUSIVE CONFIDENTIAL PROPERTY OF     ***
 ***       STRYKER CORPORATION AND ITS AFFILIATES.       ***
 ***                                                     ***
 ***             Copyright (C) 2021 Stryker              ***
 ***********************************************************/

#include <iostream>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "color_coded_mask_mouse_area.h"

int main(int argc, char* argv[])
{
    try {
        QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

        QGuiApplication app(argc, argv);

        qmlRegisterType<ColorCodedMaskMouseArea>("ColorCodedMaskMouseArea", 1, 0, "ColorCodedMaskMouseArea");   

        QQmlApplicationEngine engine;
        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
        if (engine.rootObjects().isEmpty())
            return -1;

        return app.exec();
    } catch (...) {
        std::cerr << "Error occurred" << std::endl;
    }
}